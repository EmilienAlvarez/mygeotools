import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="mygeotools",
    version="0.0.1",
    author="Emilien Alvarez-Vanhard",
    author_email="alvarez_emilien@live.fr",
    description="A list of module for remote sensing and geomatic process",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/EmilienAlvarez/mygeotools",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)