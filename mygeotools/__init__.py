# -*- coding: utf-8 -*-
"""
Created on Thu Apr 19 12:20:06 2018

@author: Emilien

MyGeoTools est une librairie de fonction pour le traitement de vecteur et raster.
"""

###############################################################################
def normalized_S2data(list_rasters, value_norm, out):
    """
    Divise chaque raster par la "value_norm"
    """
    
    import rasterio
    import os
    
    for raster in list_rasters :
        
        src = rasterio.open(raster)
        profile = src.profile
        profile.update(dtype=rasterio.float32, count=1, compress='lzw')
        array = src.read(1)/value_norm
        src.close()
        
        with rasterio.open(os.path.join(out,'normalized_{0}'.format(os.path.split(raster)[-1])), 'w', **profile) as dst:
            dst.write(array.astype(rasterio.float32), 1)
            
###############################################################################
def pts_stats_df(path_rasters, path_poly, ID_poly, classe, output_file):
    """
    """
    import rasterstats as rstats
    import pandas as pd
    import os
      
    #Création dictionnaire de base pour le dataframe de sortie
    dico_values = {}
    dico_values['ID'] = []
    dico_values['Classe'] = []
    
    #Nécessaire à l'ajout de la classe et ID dans le dataframe
    a = 1
    
    #Extraction des valeurs par polygone  
    for raster in path_rasters:
        print(raster)
        
        stats = rstats.point_query(path_poly, raster, geojson_out=True)
        
        dico_values[os.path.basename(raster)] = []
        
        for poly in stats:
            
            dico_values[os.path.basename(raster)].append(poly['properties']['value'])
            
            if a == 1:
                dico_values['Classe'].append(poly['properties'][classe])
                dico_values['ID'].append(poly['properties'][ID_poly])
        #Arret d'ajout dans les listes groupe et name
        a = a + 1

    df = pd.DataFrame(dico_values)
    df = df.sort_values(by=['Classe'])
    
    df.to_csv(os.path.join(output_file, 'sign_spec.csv'), sep = '\t', index = False)
    
###############################################################################
def zonal_stats_df(path_rasters, path_poly, stats_str, ID_poly, classe, output_file):
    """
    """
    import rasterstats as rstats
    import pandas as pd
    import os
      
    #Création dictionnaire de base pour le dataframe de sortie
    dico_values = {}
    dico_values['ID'] = []
    dico_values['Classe'] = []
    
    #Nécessaire à l'ajout de la classe et ID dans le dataframe
    a = 1
    
    #Extraction des valeurs par polygone  
    for raster in path_rasters:
        print(raster)
        
        stats = rstats.zonal_stats(path_poly, raster, stats=stats_str, geojson_out=True)
        
        dico_values[os.path.basename(raster)] = []
        
        for poly in stats:
            
            dico_values[os.path.basename(raster)].append(poly['properties'][stats_str])
            
            if a == 1:
                dico_values['Classe'].append(poly['properties'][classe])
                dico_values['ID'].append(poly['properties'][ID_poly])
        #Arret d'ajout dans les listes groupe et name
        a = a + 1

    df = pd.DataFrame(dico_values)
    df = df.sort_values(by=['Classe'])
    
    df.to_csv(os.path.join(output_file, 'sign_spec.csv'), sep = '\t', index = False)
     
###############################################################################
def extract_polygon(path_rasters, path_poly, output_file, ID_poly, classe, histo=False):
    """
    Fonction permettant d'extraire dans un dataframe et un .csv les valeurs des pixels de rasters situés dans les polygones d'un shapefile.
    Autre fonction est de produire des histogrammes par polygone et par classe
    
    -path_rasters : [Liste de String] est une liste des chemins d'accès aux rasters dont il faut extraire les valeurs
    -path_poly : [String] Chemin d'accès du shapefile contenant les polygones d'extraction
    -output_file : [String] Chemin d'accès au dossier de sortie
    -ID_poly : [String] est le nom de la colonne dans la table attributaire contenant l'ID de chaque polygone
    -classe : [String] est le nom de la colonne dans la table attributaire contenant la classe d'appartenance du polygone
    -histo : [Booléen] permet de produire ou non les histogrammes par polygone et par classe
    """
    
    # Imporation des librairies nécessaires
    import os
    from itertools import chain    
    import numpy as np    
    import pandas as pd    
    import matplotlib.pyplot as plt
    import rasterstats as rstats
    
    #Création dossiers sorties
    if histo == True :
        
        output_histo = os.path.join(output_file, 'output_histo')
        if not os.path.exists(output_histo):
            os.mkdir(output_histo)
            
        output_poly = os.path.join(output_histo, 'output_by_poly')
        if not os.path.exists(output_poly):
            os.mkdir(output_poly)
            
        output_classe = os.path.join(output_histo, 'output_by_classe')
        if not os.path.exists(output_classe):
            os.mkdir(output_classe)
        
    #Création dictionnaire de base pour le dataframe de sortie
    dico_values = {}
    dico_values['ID'] = []
    dico_values['Classe'] = []
    
    #Nécessaire à l'ajout de la classe et ID dans le dataframe
    a = 1
    
    #Extraction des valeurs par polygone  
    for raster in path_rasters:
        
        stats = rstats.zonal_stats(path_poly, raster, raster_out=True, geojson_out=True)
        
        list_values = []
        
        for poly in stats:
            
            array_sample = poly['properties']['mini_raster_array']
            
            #Transformation des arrays en liste pour intégration dans le dataframe
            array_nan = np.ma.filled(array_sample.astype(float), np.nan)
            list_sample_nan = list(chain.from_iterable(array_nan))
            list_sample_nan_cleaned = [x for x in list_sample_nan if str(x) != 'nan']
            list_values.append(list_sample_nan_cleaned)
            
            #♥Sauvegarde imagette sample + histo
            if histo == True :
                
                groupe = poly['properties'][classe]
                name = poly['properties'][ID_poly]
                title = 'Classe : {0} - ID : {1} - Bande : {2}'.format(groupe, name, os.path.split(raster)[-1])
                
                plt.figure(figsize=(10,4))
                plt.subplot(1,2,1)
                plt.imshow(array_nan)
                plt.colorbar()
                plt.subplot(1,2,2)
                plt.hist(array_nan[~np.isnan(array_nan)], bins=10, lw=0.0, stacked=False, alpha=0.3,histtype='stepfilled')
                plt.title(title)
                plt.subplots_adjust(wspace=0.4, hspace=1)
                plt.savefig(os.path.join(output_poly, 'c{0}-id{1}-{2}.png'.format(groupe, name, os.path.split(raster)[-1])), transparent=True)
                
            #Ajout dans liste classe et ID
            if a == 1:
                
                groupe = poly['properties'][classe]
                name = poly['properties'][ID_poly]
                i=0
                while i < len(list_sample_nan_cleaned):
                    i+=1
                    dico_values['Classe'].append(groupe)
                    dico_values['ID'].append(name)
            
        #Arret d'ajout dans les listes groupe et name
        a = a + 1
        
        #Fusion des listes values
        list_values_fus = list(chain.from_iterable(list_values))
        dico_values[os.path.split(raster)[-1]] = list_values_fus
        
    df = pd.DataFrame(dico_values)
    df = df.sort_values(by=['Classe'])
    
    df.to_csv(os.path.join(output_file, 'sign_spec.csv'), sep = '\t', index = False)
    
    
    #Production histogrammes
    if histo == True :
            
            #All
        pd.DataFrame.hist(df, figsize=(10,10), bins=10, grid = False)
        plt.savefig(os.path.join(output_histo, 'histo_all.png'), transparent=True)
        
            #Groupes
        groupe_val = np.unique(df['Classe'])
        
        for val in groupe_val:
            
            df_groupe = df.loc[df['Classe'] == val]
            pd.DataFrame.hist(df_groupe, figsize=(10,10), bins=10, grid = False)
            plt.savefig(os.path.join(output_classe, 'classe_{0}.png'.format(val)), transparent=True)
            
###############################################################################
def cut_raster_polys(list_raster, shape, output):
    
    """
    Découpe un raster en plusieurs subsets selon les polygones d'un shapefile
    """
    
    #Importation des librairies
    import os
    import fiona
    import rasterio
    from rasterio.tools.mask import mask

    #Ouverture shapefile
    with fiona.open(shape, "r") as shapefile:
        geoms = [feature["geometry"] for feature in shapefile]
        ID = [feature["properties"]["id"] for feature in shapefile]
    
    #Création des subsets pour chaque raster
    for raster in list_raster:
        
        for i in range(len(geoms)) :
            
            geom_list = [geoms[i]]
            
            with rasterio.open(raster) as src:
                out_image, out_transform = mask(src, geom_list, crop=True)
                out_meta = src.meta.copy()
            
            out_meta.update({"driver": "GTiff",
                             "height": out_image.shape[1],
                             "width": out_image.shape[2],
                             "transform": out_transform})
            
            output_path = os.path.join(output, "{0}_{1}".format(ID[i], raster[26:]))
            
            with rasterio.open(output_path, "w", **out_meta) as dest:
                dest.write(out_image)
                
###############################################################################
def change_datatype(raster_input, raster_output, datatype, scale, a_nodata):
    """
    scale = [String] valeurs min et max source suivi de celles de la destination
    datatype = [String] UInt16, Int32, ....
    """
    
    # Importation des librairies
    
    import os

    gdaltranslate = 'gdal_translate -ot {0} -scale {1} -a_nodata {2} -of GTiff {3} {4}'.format(datatype, scale, a_nodata, raster_input, raster_output)
    os.system(gdaltranslate)
    
        
###############################################################################
def centre_reduit(matrice):
    """
    """
    import numpy as np
    
    mean = np.nanmean(matrice)
    std = np.nanstd(matrice)
    
    matrice_CR = (matrice - mean)/std

    return matrice_CR

###############################################################################
def stack_raster(rasters, output_file, name_stack):
    """
    Stack plusieurs raster en gardant leurs noms en description
    """
    
    import os
    import rasterio
    import gdal
    
    # Read metadata of first file
    with rasterio.open(rasters[0]) as src0:
        meta = src0.meta
    
    # Update meta to reflect the number of layers
    meta.update(count = len(rasters))
    
    # Read each layer and write it to stack
    path_stack = os.path.join(output_file, '{0}.tif'.format(name_stack))
    with rasterio.open(path_stack, 'w', **meta) as dst:
        for id, layer in enumerate(rasters):
            with rasterio.open(layer) as src1:
                dst.write_band(id + 1, src1.read(1))
                
    # Ajout du nom indice dans la description des bandes
    dataset = gdal.Open(path_stack, 1)
    
    for b in range(dataset.RasterCount):
        band = dataset.GetRasterBand(b+1)
        band.SetDescription(rasters[b][:-4])
        print(band.GetDescription())
        
    dataset = None

###############################################################################
def unstack_raster(stack_path, output_file, base_name, end_name):
    """
    Unstack un stack
    end_name est une liste
    """
    
    import os
    import rasterio
    
    # Read metadata of first file
    with rasterio.open(stack_path, 'r') as src0:
        meta = src0.meta
        for i in range(1, src0.count+1):
            meta.update(count = 1)
            with rasterio.open(os.path.join(output_file,'{0}_{1}.tif'.format(base_name, end_name[i-1])), 'w', **meta) as dst:
                dst.write(src0.read(i),1)
                
###############################################################################
def create_list_raster(file):
    """
    -file : [liste de String] c'est une liste de 1 ou plusieurs fichiers contenant des rasters rangé par ordre alphanumérique
    """
    import os
    import glob
    
    list_raster = []
    
    for f in file:
        os.chdir(f)
        list_r = glob.glob('*.tif')
        list_r = [os.path.join(f,x) for x in list_r]
        for r in list_r:
            list_raster.append(r)
            
    return sorted(list_raster)

###############################################################################
def gdal_warp(path_rasters, output_file, prefix='', Xres=None, Yres=None, 
              path_mask=None, src_epsg=None, dst_epsg=None, extent = None, resampling = 'bilinear'):
    """
    Une fonction qui réutilise la ligne de commande Gdalwarp (reprojection, masking, resampling, découpe).
    Elle peut être appliqué sur une liste d'image hétérogène en résolution, SRC afin de "normalisée" le jeu de donnée.
    
    -path_rasters : [Liste de String] C'est une liste des chemins d'accès des rasters à pré-traités
    -output_file : [String] Chemin d'accès du dossier de sortie ou sera stocké les images pré-traitées
    -Xres et Yres : [Integer] Résolution spatiale souhaitée en sortie en X et Y
    -path_mask : [String] Chemin d'accès du shapefile de masquage. Ne rien indiqué si on ne veux pas de masquage
    -dst_epsg : [Integer] Numéro ESPG du SRC souhaité en sortie. Ne rien indiqué si on ne veux pas changé de système de géoréférencement
    -extent : [liste de String] C'est une liste contenant les limites de la zone dans l'ordre suivant (Xmin, Ymin, Xmax, Ymax)
    A ajouter : possiblité d'appliqué un extent (Découpage d'un subset) sans forcément appliquer un masque
    
    """
    import os, sys
    import ogr
    from osgeo.gdalconst import GA_ReadOnly
    from osgeo import osr
    import gdal
    
    #Resolution spatiale
    if Xres is not None :
        target_res = "-tr " + str(Xres) + " " + str(Yres) + " "
    else :
        target_res = ""
        
    #Pre-process masking    
    if path_mask is not None :
        
        driver = ogr.GetDriverByName("ESRI Shapefile")
    
        datasource = driver.Open(path_mask, GA_ReadOnly)
        if datasource is None :
            print ('Le shapefile ne s\'ouvre pas')
            sys.exit (1)
        
        layer = datasource.GetLayer()
        feature = layer.GetFeature(0)
        geom = feature.GetGeometryRef() 
        minX, maxX, minY, maxY = geom.GetEnvelope()
        
        #Extent masque
        target_extent = "-te " + str(minX) + " " + str(minY) + " " + str(maxX) + " " + str(maxY) + " "
        
        datasource.Destroy()
    else:
        target_extent = ""
        
    if extent is not None:
        target_extent = "-te " + str(extent[0]) + " " + str(extent[1]) + " " + str(extent[2]) + " " + str(extent[3]) + " "
        
    for path in path_rasters :
        
        #CRS d'entrée
        if src_epsg is None:
            d = gdal.Open(path)
            proj = osr.SpatialReference(wkt=d.GetProjection())
            src_epsg = proj.GetAttrValue('AUTHORITY',1)
            
        src_crs = 'EPSG:' + str(src_epsg)
        
        #CRS de sortie
        if dst_epsg is None :
            dst_crs = src_crs
        else:
            dst_crs = "EPSG:" + str(dst_epsg)
            
        target_proj = "-s_srs " + src_crs + " -t_srs " + dst_crs + " -r " + resampling + " "
        
        #Output
        path_directory, name = os.path.split(path)
        name = prefix + name
        output_path = os.path.join(output_file, name)
        
        #Appel de ligne de commande
        gdalwarp_resample = 'gdalwarp -overwrite ' + target_proj + target_res + target_extent + '-of GTiff "' + path + '" "' + output_path + '"'
        print(gdalwarp_resample)
        os.system(gdalwarp_resample)
        
        if path_mask is not None :
            
            name = "masked_" + name
            output_path_mask = os.path.join(output_file, name)
            gdalwarp_masking = 'gdalwarp -overwrite -q -cutline "' + path_mask + '" -dstnodata 0 -of GTiff "' + output_path + '" "' + output_path_mask + '"'
            os.system(gdalwarp_masking)
            os.remove(output_path)

###############################################################################
def extractPointToDF(point_shp, image, attributes):
    
    """
    Première Géo-fonction créée !!!
    
    - Fonction :
        Extrait et stocke dans un dataframe, les valeurs de toutes les bandes d'une image stackée pour
        chaque localisation des points.shp
    
    - Entrée : 
        point_shp (str) = chemin d'accès du fichier shape de localisation des valeurs à extraire dans le raster
        image (raster GDAL) = stack d'image raster représentant les variables
        attributes (liste) = liste des attributs à récupérer dans la table attributaire du fichier de point
    
    -Sortie :
        frame (dataframe pandas) = Tableau contenant les attributs et les valeurs des variables pour chaque point
        d'échantillon
        name_bands (liste) = liste des noms de chaque variable pour les identifier par la suite   
    """
    #Iportation livrairies
    import sys, math

    import ogr
    
    import pandas as pd

    # Caractéristiques image
    
    geotransform = image.GetGeoTransform()
    pixelWidth = geotransform[1]
    pixelHeight = geotransform[5]
    xOrigin = geotransform[0]
    yOrigin = geotransform[3]
    
    # Ouverture shapefile : points échantillons
    
    print("\n- Ouverture du fichier : %s" %(point_shp))
    
    driver = ogr.GetDriverByName('ESRI Shapefile')
    shapeData = driver.Open(point_shp)
    
    if shapeData is None:
        print ('Could not open shape')
        sys.exit(1)
        
    inlayer = shapeData.GetLayer()
        
    # Création dico contenant les valeurs d'échantillon pour chaque variables
    
    data = {}
        
    # extraction des classes
    
    print("- Extraction des attributs")
    
    for a in attributes :
        
        data[a]=[]
        
        for f in range(inlayer.GetFeatureCount()):
            
            feature = inlayer.GetFeature(f)
            data[a].append(feature.GetField(a))
    
     
       
    # extraction des valeurs par bandes
    
    print("- Extraction des valeurs par variables : \n")
    
    names_bands = []
    
    for b in range(1, image.RasterCount+1):
        
        print("     Process : BAND %s" %(str(b)))
        band = image.GetRasterBand(b)   
        array = band.ReadAsArray()
        
        # Recuperation nom de bande et du numéro de la bande pour faciliter la récupération de celles-ci ultérieurement
        
        num_bande = band.GetBand()
        
        names_bands.append(band.GetDescription())
        
        val_bande = []

        # Extraction des valeurs de la bande et stockage dans une liste
        
        for f in range(inlayer.GetFeatureCount()):
                    
            feature = inlayer.GetFeature(f)
            geom_point = feature.GetGeometryRef()
            
            X = geom_point.GetX()
            Y = geom_point.GetY()
            
            # Conversion de la coordonnée géographique pour retrouver le pixel sous-jacent
            i = int(math.floor((X - xOrigin)/pixelWidth))
            j = int(math.floor((Y - yOrigin)/pixelHeight))
            
            extract_val = array[j][i]
            val_bande.append(extract_val)
        
        # Stockage des valeurs de la bande dans le dico de data
            
        data[num_bande] = val_bande
        
    # Création du dataframe à partir du dico de data
    
    frame = pd.DataFrame(data)
    
    print("\n- Création du dataframe accomplie")
    
    return frame, names_bands

###############################################################################
def gdal_tile(input_raster, output_raster, col, row):
    """
    """
    
    import os, gdal
    
    ds = gdal.Open(input_raster)
    band = ds.GetRasterBand(1)
    xsize = band.XSize
    ysize = band.YSize
    
    for i in range(0, xsize, row):
        for j in range(0, ysize, col):
            com_string = "gdal_translate -of GTIFF -srcwin " + str(i)+ ", " + str(j) + ", " + str(row) + ", " + str(col) + " " + input_raster + " " + output_raster + "_" + str(i) + "_" + str(j) + ".tif"
            os.system(com_string)
            
###############################################################################
def gdal_retile(repertory, output):
    """
    Retuile les raster d'un dossier en un seul tif
    """
    
    import glob, os
    import rasterio
    import rasterio.merge
    
    os.chdir(repertory)
    
    list_tiles = glob.glob('*.tif')
    
#    gdal_retile = 'gdalwarp '
    
    list_raster =[]
    
    for tiles in list_tiles:
        
        list_raster.append(rasterio.open(tiles))
        
    
    dest, output_transform=rasterio.merge.merge(list_raster)
    print(dest.shape)
    
    with rasterio.open(list_tiles[0]) as src:
        out_meta = src.meta.copy()
        out_meta.update({"driver": "GTiff",
                     "height": dest.shape[1],
                     "width": dest.shape[2],
                     "transform": output_transform})
    
        with rasterio.open(output, "w", **out_meta) as dest1:
            dest1.write(dest)
        

    
        
#        gdal_retile = gdal_retile + os.path.join(repertory, tiles) + " "
#        
#    gdal_retile = gdal_retile + output
#        
#    print(gdal_retile)
#        
#    os.system(gdal_retile)

###############################################################################
def organize_file(directory, list_keywords, ext):
    """
    Fonction créant des dossiers contenants les fichiers avec les mots clés renseignés
    Attention aux mots-clés doublons et à l'ordre de passage dans la liste
    """
    
    import os, shutil, glob
    
    os.chdir(directory)
    
    for keyword in list_keywords:
        
        os.mkdir(keyword)
        list_files = glob.glob('*{0}*.{1}'.format(keyword, ext))
        
        for file in list_files:
            shutil.copy(file, keyword)
            os.remove(file)


###############################################################################
def scatter_RMSE(x_path, y_path, outfile, min_value=None, max_value=None, loc_error = False, hist_error = False):
    """
    Créer un .png du nuage de point avec les scores de R², RMSE et la formule de la régression
    x est la référence
    y est le test
    """
    
    import os
    import rasterio
    import matplotlib.pyplot as plt
    import numpy as np
    import scipy
    #from scipy.stats import gaussian_kde
    from sklearn.metrics import mean_squared_error
    from math import sqrt

    diry = outfile
    os.chdir(diry)
    
    #importtion image
    with rasterio.open(x_path, 'r') as src_x:
        x = src_x.read(1, masked = True)
        profile = src_x.profile
    
    with rasterio.open(y_path, 'r') as src_y:
        y = src_y.read(1, masked = True)
    
    #localisation error
    if loc_error == True:
        #error map
        a_le = np.subtract(x.astype(np.float32),y.astype(np.float32))
        profile.update(dtype=rasterio.float32, count=1, compress='lzw', nodata=0)
        
        with rasterio.open('error_localisation_{0}_{1}.tif'.format(os.path.split(x_path)[-1][:-4],
                           os.path.split(y_path)[-1][:-4]), 'w', **profile) as dst:
            dst.write(a_le.astype(rasterio.float32), 1)
        
        msk = np.where((y >= min_value) & (y <= max_value), True, False)
        x = np.where(msk, x, np.nan)
        x = x[~np.isnan(x)]
        y = np.where(msk, y, np.nan)
        y = y[~np.isnan(y)]
        
        if hist_error == True:
            #error histogram
            rmse_list = []
            
            step = (max_value - min_value)/10
            max_hist = max_value + step
            
            for i in np.arange(step,max_hist,step):
                msk = np.where((x >= i-step) & (x <= i), True, False)
                x_r = np.where(msk, x, np.nan)
                x_r = x_r[~np.isnan(x_r)]
                y_r = np.where(msk, y, np.nan)
                y_r = y_r[~np.isnan(y_r)]
                
                if x_r.shape[0] == 0:
                    rmse_list.append(0)
                else:
                    rmse_list.append(sqrt(mean_squared_error(x_r, y_r)))
    
            #plot histogram
            plt.hist(x)
            plt.plot(np.arange(step/2,max_hist-(step/2),step),rmse_list)
            plt.xlabel('abundances',fontname="Arial")
            plt.ylabel('pixel count',fontname="Arial")
            plt.ylim(min_value,max_value)
            plt.xlim(min_value,max_value)
            plt.savefig('hist_{0}_{1}.png'.format(os.path.split(x_path)[-1][:-4],
                os.path.split(y_path)[-1][:-4]), dpi=300, transparent=True)
            plt.show()
    
    else:
        msk = np.where((y >= min_value) & (y <= max_value), True, False)
        x = np.where(msk, x, np.nan)
        x = x[~np.isnan(x)]
        y = np.where(msk, y, np.nan)
        y = y[~np.isnan(y)]
    
    
    min_value = min([np.min(x), np.min(y)])
    max_value = max([np.max(x), np.max(y)])
    
    #Scores
    lr = scipy.stats.linregress(x,y)
    a = lr[0]
    b = lr[1]
    r2 = lr[2]**2
    
    RMSE = sqrt(mean_squared_error(x, y))
       
#    # Calculate the point density
#    xy = np.vstack([x,y])
#    z = gaussian_kde(xy)(xy)
#    
#    # Sort the points by density, so that the densest points are plotted last
#    idx = z.argsort()
#    x, y, z = x[idx], y[idx], z[idx]
    
    #☺Affichage
    plt.scatter(x, y, s=1, edgecolor='')
    plt.plot([min_value, max_value], [min_value, max_value], c='black')
    plt.plot([min_value, max_value], [a*min_value+b, a*max_value+b], c='r')
    plt.ylim(min_value, max_value)
    plt.xlim(min_value, max_value)
    
    plt.xlabel(os.path.split(x_path)[-1][:-4],fontname="Arial")
    plt.ylabel(os.path.split(y_path)[-1][:-4],fontname="Arial")
    
    plt.text(max_value, min_value,'y = {0:.3f}x + {1:.3f}\nR² = {2:.3f}\nRMSE = {3:.3f}'.format(a, b, r2, RMSE),
             horizontalalignment='right', verticalalignment='bottom',fontname="Arial")
    
    plt.savefig('scatter_{0}_{1}.png'.format(os.path.split(x_path)[-1][:-4],
                os.path.split(y_path)[-1][:-4]), dpi=300, transparent=True)
    plt.show()
    
###############################################################################
def random_forest(out, n_folds, pca=False,pixel_val=False):
    """
    """
    import os
    
    import pandas as pd
    import numpy as np
    
    from scipy.stats import randint as sp_randint
    
#    from sklearn.linear_model import LogisticRegression
    from sklearn.ensemble import RandomForestClassifier
#    from sklearn import svm
    from sklearn.externals import joblib
    import mygeotools as mgt
    import matplotlib.pyplot as plt
    
    os.chdir(out)
    
    df = pd.read_csv('extract_poly.csv', sep='\t')
    X = df.drop(['Unnamed: 0', 'Classe', 'ID'], 1)
    classes_int = [int(i) for i in df['Classe']]
    y = np.array(classes_int)
    
    if pca == True:
        X = mgt.acp(X,y,out)
        
    
    #Regression logistique
#    clf = LogisticRegression(solver='newton-cg', multi_class='multinomial', random_state=1, n_jobs=-1)
#    params = {'C':np.logspace(-4, 3, 10)}
#    
#    results = mgt.RandomSearchCV_stratified_group_shuffle(out, n_folds, clf, params, n_iter_search=10)
#    
#    Kappa_RD = results['m_best_kappa']
#    std_Kappa = results['std_best_kappa']
#    
#    print("LR : Kappa de %0.2f (+/- %0.2f)\n" %(Kappa_RD, std_Kappa))
#    
#    score_RD = results['m_best_OA']
#    std_score = results['std_best_OA']
#    
#    print("LR : Accuracy de %0.2f (+/- %0.2f)\n" %(score_RD, std_score))
#    
#    plt.figure()
#    mgt.plot_confusion_matrix(results['sum_best_cm'], classes=np.unique(y),
#                      title='Confusion matrix, without normalization')
#    plt.figure()
#    mgt.plot_confusion_matrix(results['sum_best_cm'], classes=np.unique(y), normalize=True,
#                      title='Normalized confusion matrix')
#    plt.show()
#    
#    model_LR = results
    
    #RF Random search   
    clf = RandomForestClassifier(n_estimators=300, random_state=1, n_jobs=-1)
    params = {"max_depth": [3, None],
              "max_features": sp_randint(1, 4),
              "min_samples_split": sp_randint(2, 11),
              "min_samples_leaf": sp_randint(1, 11),
              "bootstrap": [True, False],
              "criterion": ["gini", "entropy"]}
    
    results = mgt.RandomSearchCV_stratified_group_shuffle(out, n_folds, clf, params, n_iter_search=20, pixel_val=pixel_val)
    
    Kappa_RD = results['m_best_kappa']
    std_Kappa = results['std_best_kappa']
    
    print("RF : Kappa de %0.2f (+/- %0.2f)\n" %(Kappa_RD, std_Kappa))
    
    score_RD = results['m_best_OA']
    std_score = results['std_best_OA']
    
    print("RF : Accuracy de %0.2f (+/- %0.2f)\n" %(score_RD, std_score))
    
    plt.figure()
    mgt.plot_confusion_matrix(results['sum_best_cm'], classes=np.unique(y),
                      title='Confusion matrix, without normalization')
    plt.figure()
    mgt.plot_confusion_matrix(results['sum_best_cm'], classes=np.unique(y), normalize=True,
                      title='Normalized confusion matrix')
    plt.show()
    
    model_RF = results
    
    #importance features entropy
    importance_features(model_RF, X)
    
    #SVM   
#    Cs = [0.001, 0.01, 0.1, 1, 10, 100, 1000]
#    gammas = [0.0001, 0.001, 0.01, 0.1, 1]
#    params = {'C': Cs, 'gamma' : gammas, 'kernel' : ['rbf', 'linear', 'sigmoid', 'poly']}
#    clf = svm.SVC(probability=True, random_state=1)
#    
#    results = mgt.RandomSearchCV_stratified_group_shuffle(out, n_folds, clf, params, n_iter_search=20)
#
#    Kappa_RD = results['m_best_kappa']
#    std_Kappa = results['std_best_kappa']
#    
#    print("SVM : Kappa de %0.2f (+/- %0.2f)\n" %(Kappa_RD, std_Kappa))
#    
#    score_RD = results['m_best_OA']
#    std_score = results['std_best_OA']
#    
#    print("SVM : Accuracy de %0.2f (+/- %0.2f)\n" %(score_RD, std_score))
#    
#    plt.figure()
#    mgt.plot_confusion_matrix(results['sum_best_cm'], classes=np.unique(y),
#                      title='Confusion matrix, without normalization')
#    plt.figure()
#    mgt.plot_confusion_matrix(results['sum_best_cm'], classes=np.unique(y), normalize=True,
#                      title='Normalized confusion matrix')
#    plt.show()
#    
#    model_SVM = results
    
    # Sauvegarder le modèle utilisé
    joblib.dump(model_RF, 'RF_cv10.pkl')
#    joblib.dump(model_SVM, 'SVM_cv10.pkl')
#    joblib.dump(model_LR, 'LR_cv10.pkl')
    
    return model_RF

###############################################################################
def flatten_array(list_rasters):
    """
    """
    import rasterio
    import pandas as pd
    #Importation et transformation en vecteur de l'ensemble des images
    dico_data = {}
    
    for raster in list_rasters :
        src = rasterio.open(raster)
        array = src.read(1)
        src.close()
        dico_data[raster] = array.flatten()
    
    return pd.DataFrame(dico_data)
  
###############################################################################
def predict_image_split(model, df_data, n_split, raster_ref, title, out):
    """
    A tester !!!
    """
    import rasterio, os
    import numpy as np
    
    os.chdir(out)
    
    src = rasterio.open(raster_ref)
    
    split = np.vsplit(df_data, n_split)
    
    array_classif = model.predict(split[0])
    
    for i in range(len(1, split)):
        s_pred = model.predict(split[i])
        array_classif = np.vstack(array_classif,s_pred)
    
    array_classif = array_classif.reshape((src.height, src.width))

    #Exportation Geotiff
    with rasterio.Env():
    
        profile = src.profile
        
        profile.update(
            dtype=rasterio.uint8,
            count=1,
            nodata=255,
            compress='lzw')
    
        with rasterio.open('{0}.tif'.format(title), 'w', **profile) as dst:
            dst.write(array_classif.astype(rasterio.uint8), 1)
            
    src.close()
            
###############################################################################
def predict_image(df_data, raster, model, title, out):
    """
    """
    import rasterio, os
    
    os.chdir(out)
    
    src = rasterio.open(raster)
    array_classif = model.predict(df_data)
    array_classif = array_classif.reshape((src.height, src.width))

    #Exportation Geotiff
    with rasterio.Env():
    
        profile = src.profile
        
        profile.update(
            dtype=rasterio.uint8,
            count=1,
            nodata=255,
            compress='lzw')
    
        with rasterio.open('{0}.tif'.format(title), 'w', **profile) as dst:
            dst.write(array_classif.astype(rasterio.uint8), 1)
            
    src.close()
            
###############################################################################
def predict_proba(df_data, raster, model, title, out):
    """
    """
    import rasterio, os
    import numpy as np
    
    os.chdir(out)
    
    src = rasterio.open(raster)
    array_proba = model.predict_proba(df_data)
    
    nb_c = np.size(array_proba,1)
    
    for i in range(nb_c):
        array_classif = array_proba[:,i].reshape((src.height, src.width))

        #Exportation Geotiff
        with rasterio.Env():
            profile = src.profile
            profile.update(
                dtype=rasterio.float32,
                count=1,
                nodata=-1,
                compress='lzw')
        
            with rasterio.open('probabilite_c{0}_{1}.tif'.format(i,title), 'w', **profile) as dst:
                dst.write(array_classif.astype(rasterio.float32), 1)
    
    src.close()
            
###############################################################################
def importance_features(model_RF, X):
    """
    """
    import pandas as pd
    
        #Feature importance with RF
    fi = model_RF['best_clf'].feature_importances_
    df_fi = pd.DataFrame({'Variables' : list(X), 'FI' : fi})
    criterion = model_RF['best_clf'].criterion
    df_fi.to_csv("Feature_importances_{0}.csv".format(criterion), sep='\t')
    
###############################################################################
def spectral_sign(file, v_min=None, v_max=None):
    """
    df : id/Classe/bande_1/bande_2.../bande_n
    nom des bandes : valeurs en micromètres (ex: 200, 500, 1000)
    """
    import os
    import numpy as np
    import matplotlib.pyplot as plt
    import pandas as pd
    
    os.chdir(file)
    df = pd.read_csv('sign_spec.csv', sep='\t')
    
    classes = np.unique(df['Classe'])
    df_min = np.array(df.drop(['ID','Classe'], 1)).min()
    df_max = np.array(df.drop(['ID','Classe'], 1)).max()
    
    if v_min is not None:
        df_min = v_min
        
    if v_max is not None:
        df_max = v_max
    
    for c in classes:
        X_c = df.query('Classe == {0}'.format(c)).iloc[:,2:]
        var = [int(i) for i in list(X_c)]
        
        median = list(X_c.median())
        q_inf = list(X_c.quantile(0.25))
        q_sup = list(X_c.quantile(0.75))
        mini = list(X_c.min())
        maxi = list(X_c.max())
        
        plt.ylim(df_min, df_max)
        plt.plot(var, median, c='r')
#        plt.plot(var, q_inf, c='r', linestyle='--',alpha = 0.5)
#        plt.plot(var, q_sup, c='r', linestyle='--',alpha = 0.5)
#        plt.plot(var, mini, c='r', linestyle=':', alpha = 0.3)
#        plt.plot(var, maxi, c='r', linestyle=':', alpha = 0.3)
        plt.fill_between(var, median, q_inf, color = 'r', alpha = 0.3)
        plt.fill_between(var, median, q_sup, color = 'r', alpha = 0.3)
        plt.fill_between(var, q_inf, mini, color = 'r', alpha = 0.1)
        plt.fill_between(var, q_sup, maxi, color = 'r', alpha = 0.1)
        plt.title(c)
        plt.savefig('signature_spectrale_{0}'.format(c), dpi=300)
        plt.show()
    
    colors = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple',
              'tab:brown', 'tab:pink', 'tab:gray', 'tab:olive', 'tab:cyan']
    i=0
    for c in classes:
        X_c = df.query('Classe == {0}'.format(c)).iloc[:,2:]
        var = [int(i) for i in list(X_c)]
        
        median = list(X_c.median())
        q_inf = list(X_c.quantile(0.25))
        q_sup = list(X_c.quantile(0.75))
        mini = list(X_c.min())
        maxi = list(X_c.max())
        
        plt.ylim(df_min, df_max)
        plt.plot(var, median, c=colors[i])
        plt.plot(var, q_inf, c=colors[i], linestyle='--',alpha = 0.5)
        plt.plot(var, q_sup, c=colors[i], linestyle='--',alpha = 0.5)
        plt.plot(var, mini, c=colors[i], linestyle=':', alpha = 0.3)
        plt.plot(var, maxi, c=colors[i], linestyle=':', alpha = 0.3)
        plt.title('Signatures spectrales')
        i+= 1
    plt.savefig('signatures_spectrales'.format(c), dpi=300)
    plt.show()
        

###############################################################################
def nb_cluster(X,range_n_clusters,out, title):
    """
    X est un array ou df avec les individus en lignes et les variables en colonnes
    range_n_clusters est une liste des nombres de clusters à tester
    """

    import os
    from sklearn.cluster import KMeans
    from sklearn.metrics import silhouette_samples, silhouette_score
    
    import matplotlib.pyplot as plt
    import matplotlib.cm as cm
    import numpy as np
    
    X = np.array(X)
    for n_clusters in range_n_clusters:
        # Create a subplot with 1 row and 2 columns
        fig, (ax1, ax2) = plt.subplots(1, 2)
        fig.set_size_inches(18, 7)
    
        # The 1st subplot is the silhouette plot
        # The silhouette coefficient can range from -1, 1 but in this example all
        # lie within [-0.1, 1]
        ax1.set_xlim([-0.1, 1])
        # The (n_clusters+1)*10 is for inserting blank space between silhouette
        # plots of individual clusters, to demarcate them clearly.
        ax1.set_ylim([0, len(X) + (n_clusters + 1) * 10])
    
        # Initialize the clusterer with n_clusters value and a random generator
        # seed of 10 for reproducibility.
        clusterer = KMeans(n_clusters=n_clusters, random_state=10)
        cluster_labels = clusterer.fit_predict(X)
    
        # The silhouette_score gives the average value for all the samples.
        # This gives a perspective into the density and separation of the formed
        # clusters
        silhouette_avg = silhouette_score(X, cluster_labels)
        print("For n_clusters =", n_clusters,
              "The average silhouette_score is :", silhouette_avg)
    
        # Compute the silhouette scores for each sample
        sample_silhouette_values = silhouette_samples(X, cluster_labels)
    
        y_lower = 10
        for i in range(n_clusters):
            # Aggregate the silhouette scores for samples belonging to
            # cluster i, and sort them
            ith_cluster_silhouette_values = \
                sample_silhouette_values[cluster_labels == i]
    
            ith_cluster_silhouette_values.sort()
    
            size_cluster_i = ith_cluster_silhouette_values.shape[0]
            y_upper = y_lower + size_cluster_i
    
            color = cm.nipy_spectral(float(i) / n_clusters)
            ax1.fill_betweenx(np.arange(y_lower, y_upper),
                              0, ith_cluster_silhouette_values,
                              facecolor=color, edgecolor=color, alpha=0.7)
    
            # Label the silhouette plots with their cluster numbers at the middle
            ax1.text(-0.05, y_lower + 0.5 * size_cluster_i, str(i))
    
            # Compute the new y_lower for next plot
            y_lower = y_upper + 10  # 10 for the 0 samples
    
        ax1.set_title("The silhouette plot for the various clusters.")
        ax1.set_xlabel("The silhouette coefficient values")
        ax1.set_ylabel("Cluster label")
    
        # The vertical line for average silhouette score of all the values
        ax1.axvline(x=silhouette_avg, color="red", linestyle="--")
    
        ax1.set_yticks([])  # Clear the yaxis labels / ticks
        ax1.set_xticks([-0.1, 0, 0.2, 0.4, 0.6, 0.8, 1])
    
        # 2nd Plot showing the actual clusters formed
        colors = cm.nipy_spectral(cluster_labels.astype(float) / n_clusters)
        ax2.scatter(X[:, 0], X[:, 1], marker='.', s=30, lw=0, alpha=0.7,
                    c=colors, edgecolor='k')
    
        # Labeling the clusters
        centers = clusterer.cluster_centers_
        # Draw white circles at cluster centers
        ax2.scatter(centers[:, 0], centers[:, 1], marker='o',
                    c="white", alpha=1, s=200, edgecolor='k')
    
        for i, c in enumerate(centers):
            ax2.scatter(c[0], c[1], marker='$%d$' % i, alpha=1,
                        s=50, edgecolor='k')
    
        ax2.set_title("The visualization of the clustered data.")
        ax2.set_xlabel("Feature space for the 1st feature")
        ax2.set_ylabel("Feature space for the 2nd feature")
    
        plt.suptitle(("Silhouette analysis for KMeans clustering on sample data "
                      "with n_clusters = %d" % n_clusters),
                     fontsize=14, fontweight='bold')
    
        plt.savefig(os.path.join(out,'cl_{1}_nc_{0}.png'.format(n_clusters, title)))

###############################################################################
def ndvi(nir,red,path_out,title_out):
    """
    """
    import os
    import rasterio

    src_n = rasterio.open(nir)
    nir_a = src_n.read(1)
    src_r = rasterio.open(red)
    red_a = src_r.read(1)
    ndvi_a = (nir_a-red_a)/(nir_a+red_a)
    
    #Ecriture raster ndvi
    profile = src_n.profile
    profile.update(dtype=rasterio.float32, count=1, compress='lzw')
    
    with rasterio.open('{0}.tif'.format(os.path.join(path_out,title_out)), 'w', **profile) as dst:
        dst.write(ndvi_a.astype(rasterio.float32), 1)
    
    src_n.close()
    src_r.close()
        
###############################################################################
def ndvi_re(nir,re,path_out,title_out):
    """
    """
    import os
    import rasterio

    src_n = rasterio.open(nir)
    nir_a = src_n.read(1)
    src_r = rasterio.open(re)
    re_a = src_r.read(1)
    ndvi_a = (nir_a-re_a)/(nir_a+re_a)
    
    #Ecriture raster ndvi
    profile = src_n.profile
    profile.update(dtype=rasterio.float32, count=1, compress='lzw')
    
    with rasterio.open('{0}.tif'.format(os.path.join(path_out,title_out)), 'w', **profile) as dst:
        dst.write(ndvi_a.astype(rasterio.float32), 1)
        
    src_n.close()
    src_r.close()
        
###############################################################################
def ndwi_green(nir,green,path_out,title_out):
    """
    """
    import os
    import rasterio

    src_n = rasterio.open(nir)
    nir_a = src_n.read(1)
    src_g = rasterio.open(green)
    green_a = src_g.read(1)
    ndwi_a = (green_a-nir_a)/(green_a+nir_a)
    
    #Ecriture raster ndvi
    profile = src_n.profile
    profile.update(dtype=rasterio.float32, count=1, compress='lzw')
    
    with rasterio.open('{0}.tif'.format(os.path.join(path_out,title_out)), 'w', **profile) as dst:
        dst.write(ndwi_a.astype(rasterio.float32), 1)
        
    src_n.close()
    src_g.close()
        
###############################################################################
def identify_nodata_anomaly(list_rasters, path_out):
    """
    """
    import os
    import rasterio
    import numpy as np
    
    #initialize empty raster
    src_i = rasterio.open(list_rasters[0])
    array_i = np.zeros(src_i.shape)
    
    #Create nan map
    for raster in list_rasters :
        src = rasterio.open(raster)
        nodata = src.nodata
        array = src.read(1)
        src.close()
        m_a = np.where(array==nodata,1,0)
        array_i += m_a
    
    #Write raster output
    profile = src_i.profile
    src_i.close()
    profile.update(dtype=rasterio.uint16, count=1, compress='lzw')
    
    with rasterio.open('{0}.tif'.format(os.path.join(path_out,'nodata_anomaly')), 'w', **profile) as dst:
        dst.write(array_i.astype(rasterio.uint16), 1) 

###############################################################################             
import matplotlib.pyplot as plt
def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    import matplotlib.pyplot as plt
    import numpy as np
    import itertools
    
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else '.0f'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.tight_layout()

###############################################################################
def conf_matrix(out, n_folds, RF_model_path):
    """
    """
    import os
    import MyGeoTools as mgt
    from sklearn.metrics import confusion_matrix

    import pandas as pd
    import numpy as np
    
    from sklearn import model_selection
    from sklearn.externals import joblib
    from sklearn.ensemble import RandomForestClassifier
    
    from scipy.stats import randint as sp_randint
    
    from sklearn.model_selection import RandomizedSearchCV
    from sklearn.metrics import cohen_kappa_score, make_scorer, accuracy_score
    
    #Intègre l'indice de Kappa comme scorer dans le gridsearch
    dico_scorer = {}
    dico_scorer['Kappa'] = make_scorer(cohen_kappa_score)
    dico_scorer['Accuracy'] = make_scorer(accuracy_score)
    
    os.chdir(out)
    df = pd.read_csv('extract_poly.csv', sep='\t')
    classes_int = [int(i) for i in df['Classe']]

    X = np.array(df.drop(['Unnamed: 0', 'Classe', 'ID'], 1))
    y = np.array(classes_int)
    
    cv = model_selection.StratifiedKFold(n_splits=n_folds,shuffle=True,random_state=1)
    nb_classe = len(np.unique(y))
    cnf_matrix = np.zeros((nb_classe, nb_classe))

    for train_index, test_index in cv.split(X,y):
        X_train, X_test = X[train_index], X[test_index]
        y_train, y_test = y[train_index], y[test_index]
        
        #Classifier training
        RF_model = joblib.load(RF_model_path)
#        best_params = RF_model.best_params_
        best_estimator = RF_model.best_estimator_

        
#        clf = RandomForestClassifier(n_estimators=300)
#        param_dist = {"max_depth": [3, None],
#          "max_features": sp_randint(1, 4),
#          "min_samples_split": sp_randint(2, 11),
#          "min_samples_leaf": sp_randint(1, 11),
#          "bootstrap": [True, False],
#          "criterion": ["gini", "entropy"]}
#    
#        n_iter_search = 20
#        random_search = RandomizedSearchCV(clf, param_distributions=param_dist,
#                                       n_iter=n_iter_search, n_jobs=-1, cv=2,
#                                       scoring=dico_scorer, refit='Kappa',
#                                       random_state=1)
#        random_search.fit(X_train, y_train)
        
#        clf_opt = RandomForestClassifier(n_estimators=300, **best_params, random_state=1, n_jobs=-1)
#        clf_opt = RF_model.best_estimator_
        best_estimator.fit(X_train, y_train)
        
        #Classifier predicting
        y_pred=best_estimator.predict(X_test)
        print(cohen_kappa_score(y_test,y_pred))
        cnf_matrix += confusion_matrix(y_test, y_pred)
        print(cnf_matrix)
    #Export .csv
    df_cm = pd.DataFrame(cnf_matrix)
    df_cm.to_csv("confusion_matrix.csv", sep='\t')
    # Plot non-normalized confusion matrix
    np.set_printoptions(precision=2)
    plt.figure()
    mgt.plot_confusion_matrix(cnf_matrix, classes=np.unique(y),
                      title='Confusion matrix, without normalization')
    # Plot normalized confusion matrix
    plt.figure()
    mgt.plot_confusion_matrix(cnf_matrix, classes=np.unique(y), normalize=True,
                      title='Normalized confusion matrix')

    plt.show()
    
###############################################################################
def RandomSearchCV_stratified_group_shuffle(out, n_folds, clf_i, params, n_iter_search, pixel_val = False):
    """
    Cross val avec optimisation, OA, Kappa et matrice de confusion
    Comment ajouter une pondération par groupe ??? pondération par nombre de pixel dans le polygone
    Attention des groupes déséquilibrés peut amener un écart-type important dans les résultats du CV !!!
    Attention le nombre de groupe ne doit pas etre inférieur au nombre n_folds !!!
    
    Rajouter une entrée df pixel (quand on intégre des valeurs de médianes ou moyenne) qui permettra de prédire et valider sur les pixels et les valeurs médianes de quadrats !
    Juste prévoir un cas ou le df pixel viendrait remplacer le df de validation !!
    """
    #Ne pas oublier les random_state partout (cv, parametersample,classifier) pour pouvoir réappliquer plus tard !
    #sklearn.model_selection.ParameterSampler => pour l'optimisation !!!!
    
    from sklearn import model_selection
    import os
    import pandas as pd
    import numpy as np
    from sklearn.model_selection import ParameterSampler
    from sklearn.metrics import cohen_kappa_score
    from sklearn.metrics import confusion_matrix
    from sklearn.metrics import accuracy_score
    from copy import deepcopy
    
    os.chdir(out)
    
    df = pd.read_csv('extract_poly.csv', sep='\t')
    
    #Validation sur les pixel et non sur les médianes ou moyennes
    if pixel_val == True:
        df_pix = pd.read_csv('extract_pixel.csv', sep='\t')
        
    df_ID_classe = df.groupby('ID').agg({'Classe': np.mean})
    ID = df_ID_classe.index
    classe = df_ID_classe.Classe
    
    cv = model_selection.StratifiedKFold(n_splits=n_folds,shuffle=True,random_state=1)
    
    array_cm = []
    array_kappa = []
    array_OA = []
    array_params = []
    array_clf = []
    
    for train_index, test_index in cv.split(ID,classe):

        ID_train, ID_test = ID[train_index], ID[test_index]
        
        #Train data
        df_train = df[df['ID'].isin(ID_train)]
        classes_int_train = [int(i) for i in df_train['Classe']]
        X_train = np.array(df_train.drop(['Unnamed: 0', 'Classe', 'ID'], 1))
        y_train = np.array(classes_int_train)
        
        #Test data
        if pixel_val == True:
            df_test = df_pix[df_pix['ID'].isin(ID_test)]
        else:
            df_test = df[df['ID'].isin(ID_test)]
            
        classes_int_test = [int(i) for i in df_test['Classe']]
        X_test = np.array(df_test.drop(['Unnamed: 0', 'Classe', 'ID'], 1))
        y_test = np.array(classes_int_test)
        
        list_cm = []
        list_kappa = []
        list_OA = []
        list_params = []
        list_clf = []
        
        #Classification process
        ps = ParameterSampler(params,n_iter_search,random_state=1)
        
        for p in ps:
            clf = deepcopy(clf_i)
            clf.set_params(**p)
            clf.fit(X_train,y_train)
            
            y_pred = clf.predict(X_test)
            
            list_OA.append(accuracy_score(y_test, y_pred))
            list_kappa.append(cohen_kappa_score(y_test, y_pred))
            list_cm.append(confusion_matrix(y_test, y_pred))
            list_params.append(p)
            list_clf.append(clf)
        
        array_cm.append(list_cm)
        array_kappa.append(list_kappa)
        array_OA.append(list_OA)
        array_params.append(list_params)
        array_clf.append(list_clf)
        
    #Model Selection
    a_kappa = np.array(array_kappa)
    a_cm = np.array(array_cm)
    a_OA = np.array(array_OA)
    a_params = np.array(array_params)
    
    best_m_kappa = 0
    best_index = 0
    
    for i in range(n_iter_search):
        m_kappa = np.mean(a_kappa[:,i])
        
        if m_kappa > best_m_kappa:
            best_m_kappa = m_kappa
            best_index = i
    
    dico_results = {}    
    
    dico_results['best_params'] = a_params[0,best_index]
    
    dico_results['best_clf'] = array_clf[0][best_index]
    
    dico_results['best_kappa'] = a_kappa[:,best_index]
    dico_results['m_best_kappa'] = np.mean(dico_results['best_kappa'])
    dico_results['std_best_kappa'] = np.std(dico_results['best_kappa'])
    
    dico_results['best_OA'] = a_OA[:,best_index]
    dico_results['m_best_OA'] = np.mean(dico_results['best_OA'])    
    dico_results['std_best_OA'] = np.std(dico_results['best_OA'])
    
    dico_results['best_cm'] = a_cm[:,best_index]
    dico_results['sum_best_cm'] = np.zeros(np.shape(dico_results['best_cm'][0]))
    
    for i in range(len(dico_results['best_cm'])):
        dico_results['sum_best_cm'] += dico_results['best_cm'][i]
        
    return dico_results
    
###############################################################################   
def set_n_jobs(model, n_jobs):
    """
    """
    params = {'n_jobs':n_jobs}
    return model.set_params(**params)
        
###############################################################################
def ref_abundances(classif, ref, fe, path_out):
    """
    Ne pas oublier de masquer les classifs pour ne pas confondre les nodatas avec une classe
    fe = facteur d'échelle
    fe doit être un entier
    """
    
    import numpy as np
    import rasterio
    import os
    
    os.chdir(path_out)
    
    #image_ref
    src_r = rasterio.open(ref)
    a_r = src_r.read(1)
    rows = a_r.shape[0]
    columns = a_r.shape[1]
    
    #mask
    msk = src_r.read_masks(1)
    msk = np.where(msk==0, 20000,0)
    
    #classif
    src_c = rasterio.open(classif)
    a_c = src_c.read(1)
    src_c.close()
    
    classes = np.unique(a_c)
    
    a_abond = {}
    
    for cl in classes:
        a_abond[cl] = np.zeros(src_r.shape)+msk
     
    for c in range(columns):
        for r in range(rows):
            unique, counts = np.unique(a_c[r*fe:(r+1)*fe,c*fe:(c+1)*fe], return_counts=True)
            d_u = dict(zip(unique, counts))
            
            for cl in unique:
                a_abond[cl][r,c] = d_u[cl]*4
    
    #Write raster output
    profile = src_r.profile
    src_r.close()
    profile.update(dtype=rasterio.uint16, count=1, compress='lzw', nodata=20000)
    
    for cl in classes:
        with rasterio.open('abundances_ref_cl{0}.tif'.format(cl), 'w', **profile) as dst:
            dst.write(a_abond[cl].astype(rasterio.uint16), 1) 

###############################################################################
def sum_abundances(rep, ref, dict_end):
    """
    ex : dict_end = {0:range(1,10), 1:range(10,15), 2:range(15,22), 5:range(22,24)}
    L'image référence doit bien avoir un mask complet !
    """
    import os
    import rasterio
    import numpy as np
    
    os.chdir(rep)
    
    src_ref = rasterio.open(ref)
    
    #mask
    msk = src_ref.read_masks(1)
    msk = np.where(msk==0, 20000,0)
    
    profile = src_ref.profile
    profile.update(dtype=rasterio.uint16, count=1, compress='lzw', nodata=20000)
    
    for key, value in dict_end.items():
        a_end = np.zeros(src_ref.shape)+msk

        for v in value:
            src_e = rasterio.open(os.path.join(rep,'endmember_{0}.tif'.format(v)))
            a_end += src_e.read(1)
            
        with rasterio.open('abundances_cl{0}.tif'.format(key), 'w', **profile) as dst:
            dst.write(a_end.astype(rasterio.uint16), 1)
    
    src_ref.close()
###############################################################################
def georef_array(list_rasters, ref, out):
    """
    """
    import rasterio
    
    with rasterio.open(ref, 'r') as src_ref:
        profile = src_ref.profile
        src_crs = src_ref.crs
        
    for r in list_rasters:
        with rasterio.open(r,'r') as src:
            a = src.read(1)
        with rasterio.Env():
            profile.update(
                    dtype=rasterio.uint16,
                    count=1,
                    nodata=0,
                    crs=src_crs)
            with rasterio.open(r,'w',**profile) as dst:
                dst.write(a.astype(rasterio.uint16),1)
                
###############################################################################
def selection_signatures(file, path_out, pos=4):
    """
    selectionne les signatures dans l'interquartile
    pos : positin de la colonne nir...
    """
    import os
    import numpy as np
    import pandas as pd
    
    os.chdir(file)
    df = pd.read_csv('sign_spec.csv', sep='\t')
    
    classes = np.unique(df['Classe'])

    df_select = pd.DataFrame(columns=list(df))
    
    for c in classes:
        X_c = df.query('Classe == {0}'.format(c))

        q_inf = list(X_c.quantile(0.25))
        q_sup = list(X_c.quantile(0.75))
        
        X_q = X_c[(X_c['800'] >= q_inf[pos]) & (X_c['800'] <= q_sup[pos])]
        df_select = pd.concat([df_select, X_q])
    
    df_select.to_csv(os.path.join(path_out, 'sign_spec.csv'), sep='\t', index=False)
    return df_select

###############################################################################
def similarity_matrix(df, dist, ids, dissimilarity = False):
    """
    dist = (str) distance renseignée dans la fonction pdist
    df ne doit avoir que les colonnes de variables et celle des ids de quadrats
    """
    import pandas as pd
    from scipy.spatial.distance import squareform
    from scipy.spatial.distance import pdist
    
    res = pdist(df.drop(ids, 1), dist)
    
    if dissimilarity == False:
        sim_matrix = pd.DataFrame(1-squareform(res), index=df[ids], columns= df[ids])
    
    else :
        sim_matrix = pd.DataFrame(squareform(res), index=df[ids], columns= df[ids])

    return sim_matrix

###############################################################################
def plot_hyperplane(clf, min_x, max_x, linestyle, label):
    import numpy as np
    # get the separating hyperplane
    w = clf.coef_[0]
    a = -w[0] / w[1]
    xx = np.linspace(min_x - 5, max_x + 5)  # make sure the line is long enough
    yy = a * xx - (clf.intercept_[0]) / w[1]
    plt.plot(xx, yy, linestyle, label=label)
    
###############################################################################
def cah(df, dist, ids):
    """
    """
    from scipy.cluster import hierarchy

    Z = hierarchy.linkage(df.drop(ids, 1), 'average', dist, True)
    
    hierarchy.dendrogram(
        Z,
        leaf_rotation=0.,  # rotates the x axis labels
        leaf_font_size=10.,  # font size for the x axis labels
        orientation="left")
    
    df['groupe'] = hierarchy.cut_tree(Z,3)
    
    return df
 
###############################################################################
def acp(X,y, path_out,ids=None):
    """
    """
    import os
    import numpy as np
    import matplotlib.pyplot as plt
    from sklearn.decomposition import PCA
    from scipy.spatial import ConvexHull
#    from sklearn import preprocessing
 
#    X_norm = preprocessing.scale(X)
    
    #ACP
    pca = PCA()
    pca.fit(X, y)
    
    # Plot the PCA spectrum
    plt.figure(1, figsize=(8, 6))
    plt.clf()
    plt.axes([.2, .2, .7, .7])
    plt.plot(pca.explained_variance_ratio_, linewidth=2)
    plt.axis('tight')
    plt.xlabel('n_components')
    plt.ylabel('explained_variance_ratio')
    plt.grid()
    plt.savefig(os.path.join(path_out,'Explained_variance_ratio.png'), dpi=300)
    plt.show()
    
    comps = pca.transform(X)
    groupes = np.unique(y)
    
    #Affichage des points sur les deux premiers axes
    fig = plt.figure(1, figsize=(10, 8))
    ax = fig.add_subplot(1, 1, 1)
    
    for c, i in zip("rgbcmykrgbcmykrgbcmykrgb", groupes):
        plt.scatter(comps[np.asarray(y) == i,0], comps[np.asarray(y) == i,1], c=c, label=i, s=10)
#        plt.fill(comps[np.asarray(y) == i,0], comps[np.asarray(y) == i,1],c=c, alpha=0.3) #Rajouter une fonction qui détermine le contour du nuage en commencant par le point le plus bas et qui remonte au point le plus haut par la gauche pour redescendre au point le plus bas par la droite...
        points = comps[np.asarray(y) == i,0:2]
        hull = ConvexHull(points)
        for simplex in hull.simplices:
            plt.plot(points[simplex, 0], points[simplex, 1], c)
     
        if ids is not None:
            for j, txt in enumerate(ids[np.asarray(y) == i]):
                ax.annotate(txt, (comps[j,0], comps[j,1]))
                
    plt.xlabel('Principal Component 1 ({0:5.2f}%)'.format(pca.explained_variance_ratio_[0]*100))
    plt.ylabel('Principal Component 2 ({0:5.2f}%)'.format(pca.explained_variance_ratio_[1]*100))
    ax.axhline(linewidth=1, color='black', linestyle = '--')
    ax.axvline(linewidth=1, color='black', linestyle = '--')
    plt.grid()
    plt.legend()
    plt.savefig(os.path.join(path_out,'scatter_plot_comp_1_2.png'), dpi=300)
    plt.show()
    
    # Plot a variable factor map for the first two dimensions.
    (fig, ax) = plt.subplots(figsize=(12, 12))
    for i in range(0, len(pca.components_)):
        ax.arrow(0,
                 0,  # Start the arrow at the origin
                 pca.components_[0, i],  #0 for PC1
                 pca.components_[1, i],  #1 for PC2
                 head_width=0.02,
                 head_length=0.02)
    
        plt.text(pca.components_[0, i] + 0.05,
             pca.components_[1, i] + 0.05,
             X.columns.values[i])
    
    an = np.linspace(0, 2 * np.pi, 100)
    plt.plot(np.cos(an), np.sin(an))  # Add a unit circle for scale
    plt.axis('equal')
    ax.set_title('Variable factor map')
    ax.axhline(linewidth=1, color='black', linestyle = '--')
    ax.axvline(linewidth=1, color='black', linestyle = '--')
    plt.xlabel('Principal Component 1 ({0:5.2f}%)'.format(pca.explained_variance_ratio_[0]*100))
    plt.ylabel('Principal Component 2 ({0:5.2f}%)'.format(pca.explained_variance_ratio_[1]*100))
    plt.grid()
    plt.savefig(os.path.join(path_out,'correlation_circle_comp_1_2.png'), dpi=300)
    plt.show()
    
    return comps

###############################################################################
def create_dict_end(path_df, classe):
    """
    """
    
    import pandas as pd
    import numpy as np
    
    df = pd.read_excel(path_df)
    classes = np.unique(df[classe])
    dict_end ={}
    prev = 1
    
    for c in classes:
        count = df[df[classe]==c].count()[0]
        dict_end[c]=range(prev,prev+count)
        prev=prev+count
   
    return dict_end     
        
###############################################################################
def hist_kernel(list_rasters, out):
    """
    """
    import seaborn as sns
    import matplotlib.pyplot as plt
    import rasterio
    import numpy as np
    import os
    
    for r in list_rasters:
        src = rasterio.open(r)
        array = src.read(1)/100
        x = array.flatten()
        x = x[np.where( x > 2 )] 
        src.close()
        
        plt.figure()
        sns.kdeplot(x, shade=True)
        plt.xlim(2,100)
        name = os.path.split(r)[1][:-4]
        plt.savefig(os.path.join(out,'{0}.png'.format(name)), dpi=300)
        
    
    